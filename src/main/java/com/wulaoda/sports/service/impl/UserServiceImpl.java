package com.wulaoda.sports.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wulaoda.sports.Req.UserReq;
import com.wulaoda.sports.Req.UserSaveReq;
import com.wulaoda.sports.Req.UserUpdatePassReq;
import com.wulaoda.sports.entity.User;
import com.wulaoda.sports.exception.BusinessExceptionCode;
import com.wulaoda.sports.mapper.UserMapper;
import com.wulaoda.sports.resp.CommonResp;
import com.wulaoda.sports.resp.PageResp;
import com.wulaoda.sports.service.IUserService;
import com.wulaoda.sports.utils.CopyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-08-21
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public UserReq login(UserReq req, CommonResp<UserReq> resp) {
        //        先去通过用户名查询数据
        User userDb = selectByUserName(req.getUsername());
//        看看是否为空如果为空就提示用户名或密码不正确
        if(ObjectUtils.isEmpty(userDb)){
            resp.setMessage(BusinessExceptionCode.LOGIN_USER_ERROR.getDesc());
            resp.setSuccess(false);
        }else {
            if(userDb.getPassword().equals(req.getPassword())){
//                密码一致登陆成功
                UserReq userReq = CopyUtil.copy(userDb, UserReq.class);
                return userReq;
            }else {
//                密码不对
//                throw new ;
//                BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR)
                resp.setMessage(BusinessExceptionCode.LOGIN_USER_ERROR.getDesc());
                resp.setSuccess(false);
            }
        }
        return req;
    }

    @Override
    public UserSaveReq register(UserSaveReq req, CommonResp<UserSaveReq> resp) {
        User user = CopyUtil.copy(req, User.class);
        if(ObjectUtils.isEmpty(req.getId())){
            User userDb = selectByUserName(req.getUsername());
            if(ObjectUtils.isEmpty(userDb)){
//                新增
//                    user.setId(snowFlake.nextId());
                userMapper.insert(user);
            }else {
//                用户名已存在
                resp.setSuccess(false);
                resp.setMessage(BusinessExceptionCode.USER_LOGIN_NAME_EXIST.getDesc());
            }
        }
        return req;
    }


    //查询用户名是否存在
    public User selectByUserName(String userName){
//        构造条件
        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        将前端传来的用户名和数据库的用户名进行对比
        wrapper.lambda().eq(User::getUsername,userName);
//        发送给mapper处理看返回结果
        List<User> userList = userMapper.selectList(wrapper);
//        如果返回结果为空说明没有这个用户名
        if(CollectionUtils.isEmpty(userList)){
            return null;
//            如果有就返回这个用户名
        }else {
            return userList.get(0);
        }
    }


    @Override
    public PageResp<User> getList(UserReq req) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if(!org.springframework.util.ObjectUtils.isEmpty(req.getUsername())){
            queryWrapper.lambda().eq(User::getUsername,req.getUsername());
        }
        if(!org.springframework.util.ObjectUtils.isEmpty(req.getScreenname())){
            queryWrapper.lambda().eq(User::getScreenname,req.getScreenname());
        }


        Page<User> page = new Page<>(req.getPage(), req.getSize());
        IPage<User> userEntityIPage = userMapper.selectPage(page, queryWrapper);
        PageResp<User> pageResp = new PageResp<>();
        pageResp.setTotal(userEntityIPage.getTotal());
        pageResp.setList(userEntityIPage.getRecords());
        return pageResp;
    }

    @Override
    public void save(UserSaveReq req) {
        User user = CopyUtil.copy(req, User.class);
        if(ObjectUtils.isEmpty(req.getId())){

        }else {
            user.setPassword(null);
            userMapper.updateById(user);
        }
    }

    //    修改密码
    @Override
    public void updatePass(UserUpdatePassReq req) {
        User user = CopyUtil.copy(req, User.class);
        userMapper.updateById(user);
    }



}
