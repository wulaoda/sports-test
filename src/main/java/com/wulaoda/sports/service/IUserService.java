package com.wulaoda.sports.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wulaoda.sports.Req.UserReq;
import com.wulaoda.sports.Req.UserSaveReq;
import com.wulaoda.sports.Req.UserUpdatePassReq;
import com.wulaoda.sports.entity.User;
import com.wulaoda.sports.resp.CommonResp;
import com.wulaoda.sports.resp.PageResp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-08-21
 */
public interface IUserService extends IService<User> {

    UserReq login(UserReq req, CommonResp<UserReq> resp);

    UserSaveReq register(UserSaveReq req, CommonResp<UserSaveReq> resp);

    PageResp<User> getList(UserReq req);

    void save(UserSaveReq req);

    void updatePass(UserUpdatePassReq req);
}
