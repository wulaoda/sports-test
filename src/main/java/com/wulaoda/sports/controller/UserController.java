package com.wulaoda.sports.controller;


import com.wulaoda.sports.Req.UserReq;
import com.wulaoda.sports.Req.UserSaveReq;
import com.wulaoda.sports.Req.UserUpdatePassReq;
import com.wulaoda.sports.entity.User;
import com.wulaoda.sports.resp.CommonResp;
import com.wulaoda.sports.resp.PageResp;
import com.wulaoda.sports.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-08-21
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/login")
    public CommonResp login(@RequestBody UserReq req){
        //将前端传来的密码进行md5加密传给后端
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<UserReq> resp = new CommonResp<>();
        UserReq userReq = userService.login(req,resp);
        resp.setContent(userReq);
        return resp;
    }

    @PostMapping("/register")
    public CommonResp register(@RequestBody UserSaveReq req){
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<UserSaveReq> resp = new CommonResp<>();
        UserSaveReq userSaveReq = userService.register(req,resp);
        resp.setContent(userSaveReq);
        return resp;
    }
    @GetMapping("/getlist")
    public CommonResp list(UserReq req){
        CommonResp<PageResp<User>> resp = new CommonResp<>();
        PageResp<User> list = userService.getList(req);
        resp.setContent(list);
        return resp;
    }

    @PostMapping("/save")
    public CommonResp save(@RequestBody UserSaveReq req){
        CommonResp<User> resp = new CommonResp<>();
        userService.save(req);
        return resp;
    }


    @PostMapping("/updatePass")
    public CommonResp updatePass(@RequestBody UserUpdatePassReq req){
        //将前端传来的密码进行md5加密传给后端
        //c26be8aaf53b15054896983b43eb6a65
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<User> resp = new CommonResp<>();
        userService.updatePass(req);
        return resp;
    }


}
