package com.wulaoda.sports.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/test1")
    public void test1(){
        System.out.println("前端访问了这个方法");
    }

    @GetMapping("/test2")
    public void test2(){
        System.out.println("前端访问了这个方法");
    }
}
