package com.wulaoda.sports.mapper;

import com.wulaoda.sports.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-08-21
 */
public interface UserMapper extends BaseMapper<User> {

}
