package com.wulaoda.sports.Req;

import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-08-21
 */
@Data
public class UserReq  extends PageReq {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 网名
     */
    private String screenname;





}
