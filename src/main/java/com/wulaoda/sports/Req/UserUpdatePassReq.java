package com.wulaoda.sports.Req;

import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-08-12
 */
@Data
public class UserUpdatePassReq {


    /**
     * id
     */
    private Long id;



    /**
     * 密码
     */
    private String password;

}